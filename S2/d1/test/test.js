const { factorial } = require('../src/util.js');

// Gets the expect and assert functions from chai to be used
const {expect, assert} = require('chai');


// "it()" accepts two parameters
// string explaining what the test should do
// callback function which contains the actual test

it('test_fun_factorial_5!_is_120', () => {
	const product = factorial(5);
	expect(product).to.equal(120);
})

it('test_fun_factorial_1!_is_1', () => {
	const product = factorial(1);
	assert.equal(product, 1);
})


// Test Suites are made of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	it('test_fun_factorial_5!_is_120', () => {
	const product = factorial(5);
	expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})

})



