/*- Don’t write production code unless it is to make a failing test pass.
- Don’t write a unit test that tests for multiple conditions.
- Don’t write any more production code that is sufficient to pass one failing unit test.*/

function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
	// 5 * 4 * 3 * 2 * 1 = 120
}


function div_check(number) {
	if (number % 5 === 0 || number % 7 === 0) {
	return true;
	} else {
	return false;
	}
}

module.exports = {
  factorial: factorial,
  div_check: div_check
};